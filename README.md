# INRAe UREP Software License

[![Latest Release](https://forgemia.inra.fr/urep/dev_utils/inrae_urep_software_license/-/badges/release.svg)](https://forgemia.inra.fr/urep/dev_utils/inrae_urep_software_license/-/releases)

This project the software license used at INRAe/UREP.

By default it is a BSD-3 license, then use the file [/LICENSE](/LICENSE). (la source du texte est celle proposée par Gitlab ou [wikipedia](https://en.wikipedia.org/wiki/BSD_licenses#cite_note-FSF-ModifiedBSD-8))

If your project include dependencies which imply other license perhaps you must use another license. See [wiki](https://sites.inra.fr/site/urep/Wiki%20UREP/Licence.aspx) for more details.

## R package project

For R project you must use file [/LICENSE_forRPackage](/LICENSE_forRPackage) en rename it `LICENSE` in your project base folder. Then use copy file [/LICENSE.md](/LICENSE.md) too.

And then, fill field `License:` of your `DESCRIPTION` file with: `BSD_3_clause + file LICENSE`.

([source](https://cran.r-project.org/web/licenses/BSD_3_clause))
